
class ModuleDocFragment(object):
    DOCUMENTATION = r'''

    options:
        api_url:
            description:
                - 'This is the base url to your CheckMK instance, eg: http://localhost:80.'
            required: false
            type: str
            default: http://localhost:8080
        api_username:
            description:
                - The api_username to use for authenticating against the API
            required: false
            type: str
            default: automation
        api_secret:
            description:
                - The api_secret to use for authenticating against the API
            required: true
            type: str
    '''
