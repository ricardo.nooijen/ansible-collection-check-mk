# Copyright: (c) 2021, Ricardo Nooijen <ricardo.nooijen@proxy.nl>


import requests
import json
import copy


class CheckMKAPI:

    def __init__(self, api_url, site, api_username, api_secret):
        self.base_url = '%s/%s/check_mk/webapi.py' % (api_url, site)
        self.api_username = api_username
        self.api_secret = api_secret

    def get_default_url_params(self):
        return {
            '_username': self.api_username,
            '_secret': self.api_secret,
            'request_format': 'json',
            'output_format': 'json',
        }

    def build_host_payload(self, params):
        unset_attributes = []
        attributes = {}
        for key in ['ipaddress', 'site', 'parents', 'tag_agent', 'tag_snmp']:
            if key in params and params[key]:
                attributes[key] = params[key]
            else:
                unset_attributes += [key]

        # Prefix the following params with 'tag_'
        for key in ['address_family']:
            if key in params and params[key]:
                attributes['tag_' + key] = params[key]
            else:
                unset_attributes += ['tag_' + key]

        payload = {'hostname': params['name'], 'attributes': attributes, 'unset_attributes': unset_attributes}
        # The folder param can be '', which means the default folder
        if 'folder' in params:
            payload['folder'] = params['folder']
            payload['create_folders'] = 1 if params['create_folders'] else 0

        return payload

    ##
    # Host resource
    ##
    def get_all_hosts(self):
        payload = self.get_default_url_params()
        payload['action'] = 'get_all_hosts'
        resp_data = requests.get(self.base_url, params=payload).json()

        if resp_data.get('result_code', 1) != 0:
            raise Exception('Unexpected result_code from server. Server responded with: %s' % resp_data)

        return resp_data.get('result')

    def get_host(self, hostname):
        params = self.get_default_url_params()
        params['action'] = 'get_host'
        params['request'] = json.dumps({'hostname': hostname})

        resp_data = requests.get(self.base_url, params=params).json()
        if resp_data.get('result_code', 1) != 0:
            return None

        return resp_data.get('result')

    def add_host(self, host):
        params = self.get_default_url_params()
        params['action'] = 'add_host'

        # Drop the 'unset_attributes' since we configure a 'new' host
        host_payload = copy.deepcopy(host)
        if 'unset_attributes' in host_payload:
            host_payload.pop('unset_attributes')

        payload = {'request': json.dumps(host_payload)}
        resp_data = requests.post(self.base_url, params=params, data=payload).json()

        if resp_data.get('result_code', 1) != 0:
            raise Exception('api/add_host resulted in non-zero error code from server; %s' % resp_data)
        return True

    def edit_host(self, host):
        params = self.get_default_url_params()
        params['action'] = 'edit_host'

        # Drop the unmodifiable attributes
        host_payload = copy.deepcopy(host)
        for attr in ['folder', 'create_folders']:
            if attr in host_payload:
                host_payload.pop(attr)

        payload = {'request': json.dumps(host_payload)}
        resp_data = requests.post(self.base_url, params=params, data=payload).json()

        if resp_data.get('result_code', 1) != 0:
            raise Exception('api/edit_host resulted in non-zero error code from server; %s' % resp_data)
        return True

    def delete_host(self, host):
        params = self.get_default_url_params()
        params['action'] = 'delete_host'

        payload = {'request': json.dumps({'hostname': host['hostname']})}
        resp_data = requests.post(self.base_url, params=params, data=payload).json()

        if resp_data.get('result_code', 1) != 0:
            raise Exception('api/delete_host resulted in non-zero error code from server; %s' % resp_data)
        return True

    ##
    # Activate changes
    ##
    def activate_changes(self, mode, sites, allow_foreign_changes, comment):
        params = self.get_default_url_params()
        params['action'] = 'activate_changes'

        payload = {'request': json.dumps({
            'mode': mode,
            'sites': sites,
            'allow_foreign_changes': allow_foreign_changes,
            'comment': comment
        })}
        resp_data = requests.post(self.base_url, params=params, data=payload).json()

        if 'result_code' not in resp_data or 'result' not in resp_data:
            raise Exception('api/activate_changes resulted in unexpected response from server; %s' % resp_data)
        return (resp_data['result_code'], resp_data['result'])
