#!/usr/bin/python
# Copyright: (c) 2021, Ricardo Nooijen <ricardo.nooijen@proxy.nl>

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: check_mk_host

short_description: This module manages host resources in CheckMK

version_added: "1.0.0"

description:
    - Add, edit or remove a host in CheckMK through it's API.

options:
    site:
        description:
            - This is the name of the site for which you want to configure resources.
        required: false
        type: str
        default: cmk
    state:
        description:
            - Set to 'present' to create or edit a host and to 'absent' to purge a host
        required: false
        type: str
        choices: ['present', 'absent']
    name:
        description:
            - The name of the host to manage.
        required: true
        type: str
        aliases:
            - hostname
    folder:
        description:
            - The folder in which to create the host.
        required: false
        type: str
    create_folders:
        description:
            - Whether to create the folder provided by C(folder) it it doesn't yet exist.
        required: false
        type: bool
        default: false
    parents:
        description:
            - The host's parent host. Usefull when monitoring to this host depends on a firewall host which also exist in CheckMK.
        required: false
        type: list
    ipaddress:
        description:
            - The IP address of the host. Can be IPv4 or IPv6.
        required: false
        type: str
    address_family:
        description:
            - The address family of the provided U(ipaddress).
        required: false
        type: str
        choices: ['no-ip', 'ip-v4-only', 'ip-v6-only', 'ip-v4v6']
    tag_snmp:
        description:
            - Determines how to monitor the host using the SNMP protocol.
        required: false
        type: str
        choices: ['no-snmp', 'snmp-v1', 'snmp-v2']
    tag_agent:
        description:
            - Determines how to monitor the host using the CheckMK agent.
        required: false
        type: str
        choices: ['no-agent', 'cmk-agent', 'all-agents', 'special-agents']
    attributes:
        description:
            - Any additional parameters for the host which can't be provided by this module's parameters, can be provided using this parameter. Values set
              in this dictionary are sent to the API without any processing in this module. Please note that we update the host's attribute map with this
              dictionary which can overwrite any existing values.
        required: false
        type: dict

extends_documentation_fragment:
    - proxy.check_mk.api_auth

seealso:
    - name: CheckMK API Reference / Hosts
      description: CheckMK API Reference related to Host resources
      link: 'https://docs.checkmk.com/latest/en/web_api_references.html#hosts'

author:
    - Ricardo Nooijen (@ricardo.nooijen)
'''

EXAMPLES = r'''
- name: Create new hosts
  proxy.check_mk.check_mk_host:
    name: "{{ item }}"
    state: present
    api_secret: "{{ check_mk_api_secret }}"
  loop: "{{ groups.all }}"
  notify: Activate pending changes
- name: Purge some hosts
  proxy.check_mk.check_mk_host:
    name: "{{ item }}"
    state: absent
    api_secret: "{{ check_mk_api_secret }}"
  loop:
    - test-mysql-2
    - test-tomcat-1
  notify: Activate pending changes
'''

RETURN = r'''
name:
    description: The hostname of the host resource we're working on
    type: str
    returned: always
    sample: 'test-node-1'
'''


from ansible.module_utils.basic import AnsibleModule
from ansible_collections.proxy.check_mk.plugins.module_utils.check_mk_api import CheckMKAPI

import copy


# One-way diff of the hosts
def host_difference(hostA, hostB):
    # Diff folder (which is called Path on CheckMK response)
    if hostA['folder'] != hostB['path']:
        return True

    # Diff attributes
    for attr in hostA.get('attributes', {}):
        if attr not in hostB.get('attributes', {}):
            return True

        attrValA = hostA['attributes'][attr]
        attrValB = hostB['attributes'][attr]

        if type(attrValA) == list and len(set(attrValA) ^ set(attrValB)) != 0:
            return True
        elif attrValA != attrValB:
            return True

    # Find unset variables which are set in hostB
    if hostB.get('attributes', None):
        for unset_attr in hostA.get('unset_attributes', {}):
            if unset_attr in hostB['attributes']:
                return True

    # No differences found
    return False


# Translate request host to CheckMK representation
def convert_host_payload_to_checkmk_representation(host):
    translated_host = host
    translated_host['path'] = host['folder']

    for attr in ['folder', 'unset_attributes', 'create_folders']:
        if attr in translated_host:
            translated_host.pop(attr)

    return translated_host


def run_module():
    # Define available arguments/parameters
    module_args = dict(
        api_url=dict(type='str', required=False, default='http://localhost:8080'),
        site=dict(type='str', required=False, default='cmk'),
        api_username=dict(type='str', required=False, default='automation'),
        api_secret=dict(type='str', required=True, no_log=True),
        state=dict(default='present', choices=['absent', 'present']),
        name=dict(type='str', required=True, aliases=['hostname']),
        folder=dict(type='str', default=''),
        create_folders=dict(type='bool', default=False),
        parents=dict(type='list', default=[]),
        ipaddress=dict(type='str'),
        tag_snmp=dict(default=None, choices=['no-snmp', 'snmp-v1', 'snmp-v2']),
        address_family=dict(default=None, choices=['no-ip', 'ip-v4-only', 'ip-v6-only', 'ip-v4v6']),
        tag_agent=dict(default=None, choices=['no-agent', 'cmk-agent', 'all-agents', 'special-agents']),
        attributes=dict(type='dict', default={})
    )

    # Define ansible module
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )
    state = module.params['state']
    name = module.params['name']
    changed = False

    # Init check_mk_api class
    check_mk = CheckMKAPI(module.params['api_url'], module.params['site'],
                        module.params['api_username'], module.params['api_secret'])

    # Query for existing CheckMK host
    try:
        check_mk_host = check_mk.get_host(module.params['name'])
    except Exception as e:
        module.fail_json(msg='An error occured while querying the CheckMK API: %s' % (e))

    if state == 'present':
        check_mk_host_payload = check_mk.build_host_payload(module.params)
        check_mk_host_payload['attributes'].update(module.params['attributes'])

    if check_mk_host is not None:
        if state == 'present':
            if host_difference(check_mk_host_payload, check_mk_host):
                if module.check_mode:
                    changed = True
                else:
                    changed = check_mk.edit_host(check_mk_host_payload)
        elif state == 'absent':
            if module.check_mode:
                changed = True
            else:
                changed = check_mk.delete_host(check_mk_host)
    elif state == 'present':
        if module.check_mode:
            changed = True
        else:
            changed = check_mk.add_host(check_mk_host_payload)

    # Exit module
    if changed:
        module.exit_json(changed=True, name=name, diff={
            'before': check_mk_host if check_mk_host else '',
            'after': convert_host_payload_to_checkmk_representation(check_mk_host_payload) if state == 'present' else ''
        })
    else:
        module.exit_json(changed=changed, name=name)


def main():
    run_module()


if __name__ == '__main__':
    main()
