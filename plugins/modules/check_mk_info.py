#!/usr/bin/python
# Copyright: (c) 2021, Ricardo Nooijen <ricardo.nooijen@proxy.nl>

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: check_mk_info

short_description: This module simply retrieves infomation from CheckMK.

version_added: "1.0.0"

description: This module retrieves some available information from CheckMK which it'll return as an object for furter processing in Ansible.

options:
    site:
        description:
            - This is the name of the site for which you want to configure resources.
        required: false
        type: str
        default: cmk

extends_documentation_fragment:
    - proxy.check_mk.api_auth

author:
    - Ricardo Nooijen (@ricardo.nooijen)
'''

EXAMPLES = r'''
- name: Retrieve all available information
  proxy.check_mk.check_mk_info:
    api_secret: "{{ check_mk_api_secret }}"
  register: check_mk_info
'''

RETURN = r'''
check_mk_info:
    description: Object containing all available information
    type: dict
    returned: always
    sample:
        "check_mk_info": {
            "hosts": {
                "test-db-1": {
                    "attributes": {
                        "ipaddress": "10.0.0.1",
                        "tag_address_family": "ip-v4-only"
                    },
                    "hostname": "test-db-1",
                    "path": ""
                }
            }
        }
'''


from ansible.module_utils.basic import AnsibleModule
from ansible_collections.proxy.check_mk.plugins.module_utils.check_mk_api import CheckMKAPI


def run_module():
    # Define available arguments/parameters
    module_args = dict(
        api_url=dict(type='str', required=False, default='http://localhost:8080'),
        site=dict(type='str', required=False, default='cmk'),
        api_username=dict(type='str', required=False, default='automation'),
        api_secret=dict(type='str', required=True, no_log=True),
    )

    # Define result object
    result = dict(
        changed=False,
        check_mk_info={},
    )

    # Define ansible module
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    # Init check_mk_api class
    check_mk = CheckMKAPI(module.params['api_url'], module.params['site'],
                    module.params['api_username'], module.params['api_secret'])

    # Query current CheckMK hosts
    try:
        result['check_mk_info']['hosts'] = check_mk.get_all_hosts()
    except Exception as e:
        module.fail_json(msg='An error occured while querying the CheckMK API: %s' % (e))

    # Return result
    module.exit_json(**result)


def main():
    run_module()


if __name__ == '__main__':
    main()
