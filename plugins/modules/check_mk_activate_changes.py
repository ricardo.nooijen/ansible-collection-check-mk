#!/usr/bin/python
# Copyright: (c) 2021, Ricardo Nooijen <ricardo.nooijen@proxy.nl>

from __future__ import (absolute_import, division, print_function)
__metaclass__ = type

DOCUMENTATION = r'''
---
module: check_mk_activate_changes

short_description: This module activates pending changes in a CheckMK instance.

version_added: "1.0.0"

description:
    - This module activates any pending changes in a CheckMK instance. Other modules in this collection can edit this configuration after which you can
      activate them using the examples below.

options:
    site:
        description:
            - This is the name of the site for which you want to configure resources.
        required: false
        type: str
        default: cmk
    mode:
        description:
            - The activation mode. Use I('mode=specific') if you only want to activate changes provided by the sites listed in the C('activate_sites') option.
        required: false
        type: str
        default: specific
        choices:
            - dirty
            - specific
    activate_sites:
        description:
            - The sites for which we should activate any pending changes.
            - If not set the value from the C('site') option will be used.
        required: false
        type: list
    allow_foreign_changes:
        description:
            - Whether we should allow activation of any changes not made by our API user. When set to 'false' and the CheckMK server has pending changes from
              other users, this module will fail.
        required: false
        type: bool
        default: false
    comment:
        description:
            - Sets the comment to use for the 'Activate Changes' action in CheckMK.
        required: false
        type: str
        default: Changes activated by Ansible

extends_documentation_fragment:
    - proxy.check_mk.api_auth

seealso:
    - name: CheckMK API Reference / activate_changes
      description: CheckMK API Reference related to the 'activate_changes' action
      link: 'https://docs.checkmk.com/latest/en/web_api_references.html#activate_changes'

author:
    - Ricardo Nooijen (@ricardo.nooijen)
'''

EXAMPLES = r'''
- name: Activate pending changes
  proxy.check_mk.check_mk_activate_changes:
    api_secret: "{{ check_mk_api_secret }}"
    allow_foreign_changes: false
'''

RETURN = r'''
result:
    description: The server response
    type: str
    returned: always
    sample: '{"sites": {"cmk": {"_time_updated": 1615817936.034934, "_status_details": "Started at: 15:18:51. Finished at: 15:18:56.", "_phase": "done", "_status_text": "Activated", "_pid": 259696, "_state": "warning", "_time_ended": 1615817936.034934, "_expected_duration": 3.0877522743835453, "_time_started": 1615817931.841933, "_site_id": "cmk", "_warnings": { ... }}}}"'
'''


from ansible.module_utils.basic import AnsibleModule
from ansible_collections.proxy.check_mk.plugins.module_utils.check_mk_api import CheckMKAPI


def run_module():
    # Define available arguments/parameters
    module_args = dict(
        # API setup
        api_url=dict(type='str', default='http://localhost:8080'),
        site=dict(type='str', default='cmk'),
        api_username=dict(type='str', default='automation'),
        api_secret=dict(type='str', required=True, no_log=True),
        # Action specific
        mode=dict(default='specific', choices=['dirty', 'specific']),
        activate_sites=dict(type=list, default=[]),
        allow_foreign_changes=dict(type=bool, default=False),
        comment=dict(type='str', default='Changes activated by Ansible')
    )

    # Define ansible module
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )
    sites = module.params['activate_sites'] if module.params['activate_sites'] else [ module.params['site'] ]
    allow_foreign_changes = '1' if module.params['allow_foreign_changes'] else '0'

    # Init check_mk_api class
    check_mk = CheckMKAPI(module.params['api_url'], module.params['site'],
            module.params['api_username'], module.params['api_secret'])

    if module.check_mode:
        module.exit_json(changed=True, result={})

    # Activate any pending changes
    try:
        (rcode, result) = check_mk.activate_changes(
            module.params['mode'],
            sites,
            allow_foreign_changes,
            module.params['comment']
        )
    except Exception as e:
        module.fail_json(msg='An error occured while contacting the CheckMK API: %s' % (e))

    if rcode != 0:
        # Something went wrong..
        module.fail_json(msg="Activating changes failed! %s" % result)

    r_sites = result['sites']
    for (k, v) in r_sites.items():
        if v['_state'] == 'success':
            module.log("[%s]: changes activated successfully" % k)
        else:
            module.warn("[%s]: failed to activated changes! %s" % (k, v))

    # Exit module
    module.exit_json(changed=True, result=result if result else {})


def main():
    run_module()


if __name__ == '__main__':
    main()
