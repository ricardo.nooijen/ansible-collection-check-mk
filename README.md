# Ansible Collection - proxy.check_mk

An [Ansible](https://www.ansible.com/) collection which includes a bunch of modules to manage CheckMK resources.

## Getting started
1. Install this module  
	The easiest way to install this module is by including it in your _requirements.yml_. If you don't have this file in the root of your project, create one with the following contents:
	```
	---
	collections:
	  - git+https://gitlab.com/ricardo.nooijen/ansible-collection-check-mk.git,1.1.0
	```
	Do note that this form of including a git dependency requires Ansible >= 2.10. You can find a tar file as alternative on the release pages but this is not guaranteed to work so use with caution.
	
	Next, run Ansible Galaxy to install any missing requirements.
	```
	# Install any required collections; ansible-galaxy > 2.9
	ansible-galaxy install -r requirements.yml
	# Install any required collections; ansible-galaxy = 2.9
	ansible-galaxy collection install -r requirements.yml
	```
	
	Also have a look (here)[https://docs.ansible.com/ansible/latest/user_guide/collections_using.html#install-multiple-collections-with-a-requirements-file] for the relevant Ansible docs.
	
1. Configure CheckMK API access  
	For the API calls we need an automation user with a corresponding automation secret. This can be configured in CheckMK under _WATO_ > _Users_. You should have an default user called _automation_, but you can configure your own as well. Select the user and go to the _Security_ section. For the _Authentication_ option, you should select '_Automation secret for machine accounts_' and write down it's value. The username and this value are necessary for the API and thus to configure our module.

Also have a look (here)[https://docs.checkmk.com/latest/en/web_api.html#automation] for the official CheckMK docs regarding the automation user.

## Example play
**inventory/test.ini**
```ini
[checkmk]
test-checkmk-1

[webservers]
test-www-[1:2]

[dbservers]
test-db-[1:3]
```
**play.yml**
```yml
---
- name: Example play to manage hosts in CheckMK
  hosts: checkmk
  become: false
  vars:
    - check_mk_api_secret: 'xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx'
  tasks:
    - name: Retrieve host info
      proxy.check_mk.check_mk_info:
        api_secret: "{{ check_mk_api_secret }}"
      register: check_mk_info
    - name: Create new hosts
      proxy.check_mk.check_mk_host:
        name: "{{ item }}"
        state: present
        api_secret: "{{ check_mk_api_secret }}"
      loop: "{{ groups.all + [ 'test-purgeme-1' ] }}"	# Make sure these hostnames resolve for proper monitoring
    - name: Purge hosts not in inventory
      proxy.check_mk.check_mk_host:
        name: "{{ item }}"
        state: absent
        api_secret: "{{ check_mk_api_secret }}"
      loop: "{{ check_mk_info['check_mk_info']['hosts'].keys() | difference(groups.all) }}"
      notify: Activate pending changes
  handlers:
    - name: Activate pending changes
      proxy.check_mk.check_mk_activate_changes:
        api_secret: "{{ check_mk_api_secret }}"
        allow_foreign_changes: false
```

## External documentation
* CheckMK API: https://docs.checkmk.com/latest/en/web_api_references.html